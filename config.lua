--  100
--  _____  _             _           
-- |  __ \| |           (_)          
-- | |__) | |_   _  __ _ _ _ __  ___ 
-- |  ___/| | | | |/ _` | | '_ \/ __|
-- | |    | | |_| | (_| | | | | \__ \
-- |_|    |_|\__,_|\__, |_|_| |_|___/
--                  __/ |            
--                 |___/             
lvim.plugins = {
  {
    "lervag/vimtex",
    lazy=false
  },
{"luk400/vim-jukit"},
{'TobinPalmer/pastify.nvim',
  cmd = { 'Pastify' },
  config = function()
    require('pastify').setup {
      opts = {
        apikey = 'ebe6cd9767dbcaf3ed1e4a895cd32413', -- Needed if you want to save online. 
        save= 'online'
      },
    }
  end},
  {
        'barrett-ruth/live-server.nvim',
        build = 'yarn global add live-server',
        config = true
    },
{
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = function() vim.fn["mkdp#util#install"]() end,
},
  {

  "rcarriga/nvim-notify",   -- optional
  },
{"kdheepak/lazygit.nvim",
        -- optional for floating window border decoration
        dependencies = {
            "nvim-lua/plenary.nvim",
        },},
  {
  "epwalsh/obsidian.nvim",
  version = "*",
  lazy = true,
  ft = "markdown",
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  opts = {
    workspaces = {
        {name = "personal",
        path = "~/Desktop/obsidian",
          overrides={
            disable_frontmatter=true,
          }
      },
      {name= "me",
        path = "~/Desktop/me",
        }
    },
  },
},
{"3rd/image.nvim",
    opts = {
            backend = "kitty", -- whatever backend you would like to use
            max_width = 1024,
            max_height = 1024,
            max_height_window_percentage = math.huge,
            max_width_window_percentage = math.huge,
            window_overlap_clear_enabled = true, -- toggles images when windows are overlapped
            window_overlap_clear_ft_ignore = { "cmp_menu", "cmp_docs", "" },
        },
   init = function()
        package.path = package.path .. ";" .. vim.fn.expand("$HOME") .. "/.luarocks/share/lua/5.1/?/init.lua;"
        package.path = package.path .. ";" .. vim.fn.expand("$HOME") .. "/.luarocks/share/lua/5.1/?.lua;"
        require("image").setup()
    end,},
{"eandrju/cellular-automaton.nvim"},
  {"mg979/vim-visual-multi"},
  {"tpope/vim-fugitive"},
}

-- _              _     _           _     
--| |            | |   (_)         | |    
--| | _____ _   _| |__  _ _ __   __| |___ 
--| |/ / _ \ | | | '_ \| | '_ \ / _` / __|
--|   <  __/ |_| | |_) | | | | | (_| \__ \
--|_|\_\___|\__, |_.__/|_|_| |_|\__,_|___/
--           __/ |                        
--          |___/                         
local map = vim.keymap.set
local expr_options = { expr = true, silent = true }
--Remap for dealing with visual line wraps
map("n", "k", "v:count == 0 ? 'gk' : 'k'", expr_options)
map("n", "j", "v:count == 0 ? 'gj' : 'j'", expr_options)

lvim.keys.normal_mode["<Space>t"] = ":PasteImg<CR>"
lvim.keys.normal_mode["<Space>kq"] = ":let g:jukit_shell_cmd='ipython'<cr>"
lvim.keys.normal_mode["<Space>kw"] = ":let g:jukit_shell_cmd='julia'<cr>"
lvim.keys.normal_mode["<Space>ke"] = ":let g:jukit_shell_cmd='R'<cr>"
vim.api.nvim_set_keymap('n', '<F6>', "<cmd>MarkdownPreview<cr>", { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<C-Enter>', "<cmd>MarkdownPreview<cr>", { noremap = true, silent = true })
 lvim.keys.normal_mode["<Enter>"] = ":ObsidianFollowLink<CR>"
-- paste image with Space t
 lvim.keys.normal_mode["<j>"] = "<g-j>"
 lvim.keys.normal_mode["<k>"] = "<g-k>"
 lvim.keys.normal_mode["<C-s>"] = ":w<CR>"
 lvim.keys.normal_mode["<Tab>"] = ":BufferLineCycleNext<CR>"
 lvim.keys.normal_mode["<S-Tab>"] = ":BufferLineCyclePrev<CR>"
 lvim.keys.normal_mode["<C-W>w"] = ":bw<CR>"
 lvim.keys.normal_mode["<C-f>"] = ":Telescope find_files<CR>"
 lvim.keys.normal_mode["<C-t>"] = ":Pastify<CR>"
lvim.builtin.cmp.preselect = require "cmp.types.cmp".PreselectMode.None
lvim.lsp.buffer_mappings.normal_mode['0'] = nil
lvim.keys.normal_mode["0"] = "$"
vim.cmd[[nnoremap <C-E> :call jukit#send#line()<cr>]]
local opt = vim.opt
opt.wrap = true
vim.opt.relativenumber = true
vim.opt.conceallevel = 1
